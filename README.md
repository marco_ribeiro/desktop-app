# Desktop App Demo

A GWT Application emulation a desktop application.

Demo purpose

## Running Locally

Make sure you have Java and Maven installed.  Also, install the [Heroku Toolbelt](https://toolbelt.heroku.com/).

```sh
$ git clone git@heroku.com:peaceful-wildwood-5715.git
$ cd peaceful-wildwood-5715
$ mvn install
$ foreman start web
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```sh
$ heroku create
$ git push heroku master
$ heroku open
```

## Documentation

For more information about using Java on Heroku, see these Dev Center articles:

- [Java on Heroku](https://devcenter.heroku.com/categories/java)

Any question please contact me:

marco.ribeiro.ext@eurocontrol.int