package eurocontrol.sesarwp13.client.ui.menu;

import com.google.gwt.user.client.Command;

/**
 * Created by mribeiro on 25/09/14.
 */
public abstract class MenuHandler {
    public abstract void execute(String id);
}
