/**
 * $$Id$$
 * @author mribeiro
 * @date 12/09/14 12:13
 *
 * Copyright (C) 2014 BTC-ATM
 *
 * All rights reserved.
 *
 */
package eurocontrol.sesarwp13.client.ui.menu;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import java.util.HashMap;

/**
 *
 */
public class TopMenuBar extends ResizeComposite {

    private LayoutPanel layoutPanel;

    private MenuHandler menuHandler;

    public TopMenuBar(MenuHandler menuHandler){
        this.menuHandler = menuHandler;
        init();
    }

    private void init(){
        layoutPanel = new LayoutPanel();

        FlowPanel flowPanel = new FlowPanel();

        // Create a menu bar
        MenuBar menu = new MenuBar();
        menu.setAutoOpen(true);
        menu.setWidth("100%");
        menu.setAnimationEnabled(true);

        // Create the file menu
        MenuBar fileMenu = new MenuBar(true);

        fileMenu.setAnimationEnabled(true);
        menu.addItem(new MenuItem("File", fileMenu));
        String[] fileOptions = {"New Workspace"};
        for (int i = 0; i < fileOptions.length; i++) {
            fileMenu.addItem(fileOptions[i], new MenuCommand(fileOptions[i], menuHandler));
        }

        // Create workspace menu
        MenuBar workspaceMenu = new MenuBar(true);
        menu.addItem(new MenuItem("Workspace", workspaceMenu));
        String[] workspaceOptions = {"New Window"};
        for (int i = 0; i < workspaceOptions.length; i++) {
            workspaceMenu.addItem(workspaceOptions[i], new MenuCommand(workspaceOptions[i], menuHandler));
        }

        // Create the help menu
        MenuBar helpMenu = new MenuBar(true);
        menu.addSeparator();
        menu.addItem(new MenuItem("Help", helpMenu));
        String[] helpOptions = {"About"};
        for (int i = 0; i < helpOptions.length; i++) {
            helpMenu.addItem(helpOptions[i], new MenuCommand(helpOptions[i], menuHandler));
        }

        // Return the menu
        menu.ensureDebugId("cwMenuBar");
        flowPanel.add(menu);
        layoutPanel.add(flowPanel);
        initWidget(layoutPanel);

    }
}