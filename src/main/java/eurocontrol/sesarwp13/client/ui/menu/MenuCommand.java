package eurocontrol.sesarwp13.client.ui.menu;

import com.google.gwt.user.client.Command;

/**
 * Created by mribeiro on 26/09/14.
 */
public class MenuCommand implements Command{
    private String id;
    private MenuHandler menuHandler;

    public MenuCommand(String id, MenuHandler menuHandler){
        this.id = id;
        this.menuHandler = menuHandler;
    }

    @Override
    public void execute() {
        menuHandler.execute(id);
    }
}
