package eurocontrol.sesarwp13.client.ui.image;

import com.google.gwt.core.client.GWT;
import eurocontrol.sesarwp13.client.ui.image.icons.Icons;

/**
 * Created by mribeiro on 23/09/14.
 */
public class ImageBundle {

    public static final Icons ICONS = GWT.create(Icons.class);
}
