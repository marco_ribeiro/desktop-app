package eurocontrol.sesarwp13.client.ui.image.icons;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Created by mribeiro on 23/09/14.
 */
public interface Icons extends ClientBundle {

    @Source("cancel.png")
    ImageResource cancel();

    @Source("application_put.png")
    ImageResource applicationPut();

    @Source("application_get.png")
    ImageResource applicationGet();
}
