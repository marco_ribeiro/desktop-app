package eurocontrol.sesarwp13.client.ui.sytle;

import com.google.gwt.resources.client.CssResource;

/**
 * Created by mribeiro on 26/09/14.
 */
public interface MyStyle extends CssResource {

    String titleStyle();

    String titleButtonsStyle();

    String titleLabelStyle();

    String panelStyle();

    String roundedCorners();

    String topRoundedCorners();

    String innerPanelButton();

    String innerPanelRoundedButton();

    String innerPanelButtonHover();
}