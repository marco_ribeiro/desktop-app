package eurocontrol.sesarwp13.client.ui.sytle;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

/**
 * Created by mribeiro on 26/09/14.
 */
public interface StyleBundle extends ClientBundle {
    @Source("InnerPanel.css")
    MyStyle myStyle();

    public static final StyleBundle INSTANCE = GWT.create(StyleBundle.class);
}
