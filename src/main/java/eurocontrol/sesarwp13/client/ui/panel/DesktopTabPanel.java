/**
 * $$Id$$
 * @author mribeiro
 * @date 10/10/14 16:49
 *
 * Copyright (C) 2014 BTC-ATM
 *
 * All rights reserved.
 *
 */
package eurocontrol.sesarwp13.client.ui.panel;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class DesktopTabPanel extends LayoutPanel {

    private int minimizedWidgets;
    private double minimizedWidgetsWidth;
    private Map<String, InnerPanel> widgets;

    public DesktopTabPanel() {
        setSize("100%", "100%");
        setStyleName("desktop");
        this.minimizedWidgets = 0;
        this.minimizedWidgetsWidth = 0.0;
        widgets = new HashMap<String, InnerPanel>();
    }

    @Override
    public void add(Widget widget) {
        super.add(widget);
        if (widget.getElement().getId() == null) {
            widget.getElement().setId(DOM.createUniqueId());
        }
        widgets.put(widget.getElement().getId(), (InnerPanel) widget);
    }

    public void minimizeWidget(InnerPanel innerPanel) {
        innerPanel.getElement().getStyle().setLeft(minimizedWidgetsWidth + (minimizedWidgets > 0 ? 5 : 0), Style.Unit.PX);
        minimizedWidgetsWidth += innerPanel.getElement().getClientWidth();
        minimizedWidgets++;
    }

    public void maximizeWidget(InnerPanel innerPanel) {
        minimizedWidgets--;
        minimizedWidgetsWidth -= innerPanel.getElement().getClientWidth();
        reorderMinimized();
    }

    private void reorderMinimized() {
        int old_minimizedWidgets = minimizedWidgets;
        minimizedWidgets = 0;
        minimizedWidgetsWidth = 0.0;
        for (InnerPanel innerPanel : widgets.values()) {
            if (innerPanel.isMinimized()) {
                minimizeWidget(innerPanel);
            }
            //all work done?
            if (minimizedWidgets == old_minimizedWidgets) {
                return;
            }
        }
    }


}