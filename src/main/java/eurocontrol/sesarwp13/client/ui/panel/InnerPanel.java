/**
 *
 */
package eurocontrol.sesarwp13.client.ui.panel;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.*;
import eurocontrol.sesarwp13.client.ui.effects.ElementFader;
import eurocontrol.sesarwp13.client.ui.image.ImageBundle;
import eurocontrol.sesarwp13.client.ui.sytle.StyleBundle;

/**
 * A simple inner panel that has a title bar and a box outline
 * (Similar to that of an inner window)
 */
public class InnerPanel extends ResizeComposite {

    public static final String TITLE_HEIGHT = "2em";
    private DraggableMouseListener draggableListener;
    private ResizableMouseListener resizableListener;
    private ElementFader elementFader = new ElementFader();

    private LayoutPanel layoutPanel;
    private Widget innerWidget;
    private Label label;
    private StyleBundle bundle;
    private FlowPanel titleBarPanel;
    private LayoutPanel contentPanel;
    private String id;
    private Hyperlink closeButton;
    private Hyperlink minimizeButton;
    private Hyperlink maximizeButton;
    private String left;
    private String bottom;
    private String top;

    private String currentLeft;
    private String currentBottom;
    private String currentTop;

    private String originalHeight;
    private InnerPanelHandler minimizeHandler;
    private InnerPanelHandler maximizeHandler;
    private InnerPanelHandler selectWindowHandler;
    private boolean minimized = false;


    public InnerPanel(String id) {
        this(StyleBundle.INSTANCE);
        this.id = id;
        this.getElement().setId(id);
    }

    public InnerPanel(StyleBundle bundle) {
        this.bundle = bundle;
        initInnerPanel();
        addButtonsHandlers();
        getElement().getStyle().setPosition(Style.Position.ABSOLUTE);
        // We don’t want to lose anything already sunk,
        // so just ORing it to what’s already there.
        DOM.sinkEvents(titleBarPanel.getElement(), DOM.getEventsSunk(titleBarPanel.getElement())
                | Event.MOUSEEVENTS);
        DOM.sinkEvents(layoutPanel.getElement(), DOM.getEventsSunk(layoutPanel.getElement())
                | Event.MOUSEEVENTS);

        draggableListener = new DraggableMouseListener();
        titleBarPanel.addDomHandler(draggableListener, MouseOverEvent.getType());
        titleBarPanel.addDomHandler(draggableListener, MouseUpEvent.getType());
        titleBarPanel.addDomHandler(draggableListener, MouseDownEvent.getType());
        titleBarPanel.addDomHandler(draggableListener, MouseMoveEvent.getType());

        resizableListener = new ResizableMouseListener();
        contentPanel.addDomHandler(resizableListener, MouseUpEvent.getType());
        contentPanel.addDomHandler(resizableListener, MouseDownEvent.getType());
        contentPanel.addDomHandler(resizableListener, MouseMoveEvent.getType());

    }

    /**
     * just to debug to javascript console
     *
     * @param text
     */
    public static native void console(String text)
    /*-{
        console.log(text);
    }-*/;

    public void setMinimizeHandler(InnerPanelHandler handler) {
        this.minimizeHandler = handler;
    }

    public void setMaximizeHandler(InnerPanelHandler maximizeHandler) {
        this.maximizeHandler = maximizeHandler;
    }

    public void setPanelTitle(String title) {
        label.setText(title);
    }

    @Override
    public void setWidth(String width) {
        super.setWidth(width);
        layoutPanel.setWidth(width);
    }

    @Override
    public void setHeight(String height) {
        super.setHeight(height);
        layoutPanel.setHeight(height);
    }

    public void setInnerWidget(Widget widget) {
        if (innerWidget != null)
            throw new IllegalStateException("InnerPanel.setInnerWidget may only be called once.");

        this.innerWidget = widget;
        contentPanel.add(innerWidget);
    }

    public int getZIndex() {
        try {
            return Integer.parseInt(getElement().getStyle().getZIndex());
        } catch (NumberFormatException e) {
            return Integer.MIN_VALUE;
        }
    }

    public void saveCurrentPosition() {
        currentTop = getElement().getStyle().getTop();
        currentLeft = getElement().getStyle().getLeft();
        currentBottom = getElement().getStyle().getBottom();
        console(id + "|| saving top: " + currentTop + ", left: " + currentLeft + ", bottom: " + currentBottom);
    }

    public void restorePosition(){
        console(id + "|| restoring top: " + currentTop + ", left: " + currentLeft + ", bottom: " + currentBottom);
        getElement().getStyle().setTop(getStyleValue(currentTop), getStyleUnit(currentTop));
        getElement().getStyle().setBottom(getStyleValue(currentBottom), getStyleUnit(currentBottom));
        getElement().getStyle().setLeft(getStyleValue(currentLeft), getStyleUnit(currentLeft));
    }

    public void setSelectWindowHandler(InnerPanelHandler handler) {
        this.selectWindowHandler = handler;
    }

    public boolean isMinimized() {
        return minimized;
    }

    private void minimize() {
        minimized = true;
        //hide minimize button and show maximize button
        setMinMaxButtonsVisible(false);

        //save current height and position
        originalHeight = getElement().getStyle().getHeight();
        this.setHeight(TITLE_HEIGHT);
        left = getElement().getStyle().getLeft();
        bottom = getElement().getStyle().getBottom();
        top = getElement().getStyle().getTop();

        //set the position to bottom left corner
        getElement().getStyle().setLeft(getOffsetWidth(), Style.Unit.PX);
        getElement().getStyle().setBottom(0, Style.Unit.PCT);
        getElement().getStyle().setTop(95, Unit.PCT);
//        getElement().getStyle().clearTop();

        //execute the desktop panel handler if exists
        //it's possible that there are another better way of doing this
        //this is to manage left value depending of current minimized windows
        if (minimizeHandler != null) {
            minimizeHandler.execute(this);
        }

    }

    private void maximize() {
        minimized = false;
        //hide maximize button and show minimize button
        setMinMaxButtonsVisible(true);

        //restore previous height and position
        this.setHeight(originalHeight);
        getElement().getStyle().setLeft(getStyleValue(left), getStyleUnit(left));
        getElement().getStyle().setBottom(getStyleValue(bottom), getStyleUnit(bottom));
        getElement().getStyle().setTop(getStyleValue(top), getStyleUnit(top));

        //execute the desktop panel handler if exists
        //it's possible that there are another better way of doing this
        //this is to update the number of minimized windows
        if (maximizeHandler != null) {
            maximizeHandler.execute(this);
        }
    }

    /**
     * Returns the unit of a style property value
     *
     * @param propertyValue String from style property
     * @return Unit enum
     */
    private Unit getStyleUnit(String propertyValue) {
        String regex = "\\D+";

        RegExp regExp = RegExp.compile(regex);
        MatchResult matcher = regExp.exec(propertyValue);
        boolean matchFound = matcher != null;

        String unitStr = "px";

        if (matchFound) {
            if (matcher.getGroupCount() == 1) {
                unitStr = matcher.getGroup(0).toUpperCase();
            } else {
                unitStr = matcher.getGroup(1).toUpperCase();
            }
        }

        //because Unit.valueOf is not properly implemented
        for (Unit unit : Unit.values()) {
            if (unit.getType().equalsIgnoreCase(unitStr)) {
                return unit;
            }
        }

        return Unit.PX;
    }

    /**
     * Returns the double value of a style property value.
     *
     * @param propertyValue String from style property
     * @return double
     */
    private double getStyleValue(String propertyValue) {
        String regex = "\\d+\\.?\\,?\\d*";

        RegExp regExp = RegExp.compile(regex);
        MatchResult matcher = regExp.exec(propertyValue);
        boolean matchFound = matcher != null;

        if (matchFound && matcher.getGroupCount() == 1) {
            return Double.parseDouble(matcher.getGroup(0));
        }

        return 0;
    }

    private void setMinMaxButtonsVisible(boolean value) {
        minimizeButton.setVisible(value);
        maximizeButton.setVisible(!value);
    }

    private void initInnerPanel() {
        bundle.myStyle().ensureInjected();

        layoutPanel = new LayoutPanel();
        titleBarPanel = new FlowPanel();
        //titleBarPanel.getElement().getStyle().setProperty("textAlign", "center");

        label = new Label();
        label.addStyleName(bundle.myStyle().titleLabelStyle());
        titleBarPanel.add(label);
        HorizontalPanel buttons = new HorizontalPanel();

        minimizeButton = new Hyperlink();
        Image imageMinimize = new Image(ImageBundle.ICONS.applicationPut());
        imageMinimize.setStyleName(bundle.myStyle().innerPanelButton());
        imageMinimize.setStyleName(bundle.myStyle().innerPanelButtonHover());
        imageMinimize.setAltText("Minimize");
        minimizeButton.getElement().appendChild(imageMinimize.getElement());
        buttons.add(minimizeButton);

        maximizeButton = new Hyperlink();
        maximizeButton.setVisible(false);
        Image imageMaximize = new Image(ImageBundle.ICONS.applicationGet());
        imageMaximize.setStyleName(bundle.myStyle().innerPanelButton());
        imageMaximize.setStyleName(bundle.myStyle().innerPanelButtonHover());
        imageMaximize.setAltText("Maximize");
        maximizeButton.getElement().appendChild(imageMaximize.getElement());
        buttons.add(maximizeButton);

        closeButton = new Hyperlink();
        Image imageClose = new Image(ImageBundle.ICONS.cancel());
        imageClose.setStyleName(bundle.myStyle().innerPanelRoundedButton());
        imageClose.setAltText("Close");
        closeButton.getElement().appendChild(imageClose.getElement());
        buttons.add(closeButton);

        titleBarPanel.add(buttons);
        buttons.addStyleName(bundle.myStyle().titleButtonsStyle());

        layoutPanel.add(titleBarPanel);
        layoutPanel.setWidgetTopHeight(titleBarPanel, 0.0, Unit.PCT, 2, Unit.EM);
        layoutPanel.setWidgetLeftRight(titleBarPanel, 0.0, Unit.PCT, 0.0, Unit.PCT);
        contentPanel = new LayoutPanel();

        layoutPanel.add(contentPanel);
        layoutPanel.setWidgetTopBottom(contentPanel, 2, Unit.EM, 0.0, Unit.PCT);
        layoutPanel.setWidgetLeftRight(contentPanel, 0.0, Unit.PCT, 0.0, Unit.PCT);
        //layoutPanel.setWidgetHorizontalPosition(titleBarPanel, Alignment.STRETCH);

        initWidget(layoutPanel);
        layoutPanel.getElement().getStyle().setPosition(Style.Position.RELATIVE);
        layoutPanel.setStyleName(bundle.myStyle().panelStyle());
        layoutPanel.addStyleName(bundle.myStyle().roundedCorners());
        layoutPanel.getElement().getStyle().setBackgroundColor("aliceblue");
        titleBarPanel.setStyleName(bundle.myStyle().titleStyle());
        titleBarPanel.addStyleName(bundle.myStyle().topRoundedCorners());

    }

    private void addButtonsHandlers() {
        closeButton.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                destroyWidget();
            }
        }, ClickEvent.getType());

        minimizeButton.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                minimize();
            }
        }, ClickEvent.getType());

        maximizeButton.addDomHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                maximize();
            }
        }, ClickEvent.getType());
    }

    private void destroyWidget() {
        final InnerPanel toDestroy = this;
        elementFader.fade(getElement(), 1.0f, 0.0f, 500, new ElementFader.FaderCallback() {
            @Override
            public void fadeEnd() {
                toDestroy.removeFromParent();
            }
        });


    }

    private void selectWindow() {
        if (selectWindowHandler != null) {
            selectWindowHandler.execute(this);
        }
    }

    private class DraggableMouseListener implements MouseDownHandler, MouseUpHandler, MouseMoveHandler, MouseOverHandler {

        private boolean dragging = false;
        private int dragStartX;
        private int dragStartY;

        public void onMouseDown(MouseDownEvent event) {
            event.preventDefault();
            event.stopPropagation();
            dragging = true;
            DOM.setCapture(titleBarPanel.getElement());
            dragStartX = event.getRelativeX(titleBarPanel.getElement());
            dragStartY = event.getRelativeY(titleBarPanel.getElement());
            selectWindow();
        }

        public void onMouseUp(MouseUpEvent event) {
            dragging = false;
            DOM.releaseCapture(titleBarPanel.getElement());
            getElement().getParentElement().getStyle().setCursor(Style.Cursor.DEFAULT);
        }

        public void onMouseMove(MouseMoveEvent event) {
            if (dragging) {
                event.preventDefault();
                event.stopPropagation();
                double left = getStyleValue(getElement().getStyle().getLeft());
                double top = getStyleValue(getElement().getStyle().getTop());
                double newX = Math.max(0, event.getRelativeX(titleBarPanel.getElement()) + left - dragStartX);
                double newY = Math.max(0, event.getRelativeY(titleBarPanel.getElement()) + top - dragStartY);
                getElement().getStyle().setLeft(newX, Style.Unit.PX);
                getElement().getStyle().setTop(newY, Style.Unit.PX);
                getElement().getParentElement().getStyle().setCursor(Style.Cursor.MOVE);
            }

        }

        public void onMouseOver(MouseOverEvent event) {
            titleBarPanel.getElement().getStyle().setCursor(Style.Cursor.MOVE);
        }
    }

    private class ResizableMouseListener implements MouseDownHandler, MouseUpHandler, MouseMoveHandler {

        private boolean resize = false;

        public void onMouseDown(MouseDownEvent event) {
            if (isCursorResize(event)) {
                event.preventDefault();
                event.stopPropagation();
                resize = true;
                DOM.setCapture(contentPanel.getElement());
                selectWindow();
            }

        }

        public void onMouseUp(MouseUpEvent event) {
            if (isCursorResize(event)) {
                resize = false;
                DOM.releaseCapture(contentPanel.getElement());
                getElement().getStyle().setCursor(Style.Cursor.DEFAULT);
            }
        }

        public void onMouseMove(MouseMoveEvent event) {

            if (isCursorResize(event))
                getElement().getStyle().setCursor(Style.Cursor.SE_RESIZE);
            else
                getElement().getStyle().setCursor(Style.Cursor.DEFAULT);

            if (resize) {
                event.preventDefault();
                event.stopPropagation();
                int absX = event.getClientX();
                int absY = event.getClientY();
                int originalX = getElement().getAbsoluteLeft();
                int originalY = getElement().getAbsoluteTop();

                //do not allow mirror-functionality
                if (absY > originalY && absX > originalX) {
                    Integer height = absY - originalY + 2;
                    setHeight(height + "px");

                    Integer width = absX - originalX + 2;
                    setWidth(width + "px");
                }
            }

        }

        /**
         * returns if mousepointer is in region to show cursor-resize
         *
         * @param event
         * @return true if in region
         */
        private boolean isCursorResize(MouseEvent event) {
            int cursorY = event.getClientY();
            int initialY = getElement().getAbsoluteTop();
            int height = getElement().getOffsetHeight();

            int cursorX = event.getClientX();
            int initialX = getElement().getAbsoluteLeft();
            int width = getElement().getOffsetWidth();

            //only in bottom right corner (area of 10 pixels in square)
            if (((initialX + width - 10) < cursorX && cursorX <= (initialX + width)) &&
                    ((initialY + height - 10) < cursorY && cursorY <= (initialY + height)))
                return true;
            else
                return false;
        }

    }

}
