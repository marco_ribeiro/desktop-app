/**
 * $$Id$$
 * @author mribeiro
 * @date 12/09/14 15:53
 *
 * Copyright (C) 2014 BTC-ATM
 *
 * All rights reserved.
 *
 */
package eurocontrol.sesarwp13.client.ui.panel.resizable;

import com.google.gwt.event.dom.client.MouseEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 */
public class ResizableDialogBox extends DialogBox {

    public ResizableDialogBox(boolean autoHide, boolean modal) {
        super(autoHide, modal);
    }

    @Override
    public void setWidget(final Widget w) {
        super.setWidget(w);
        w.getParent().addDomHandler(new MouseOverHandler() {
            public void onMouseOver(MouseOverEvent event) {
                /*if(isCursorResize(event, w)){
                    System.out.println("resize");
                    DOM.setStyleAttribute(w.getParent().getElement(), "cursor", "se-resize");
                }else {
                    System.out.println("non-resize");
                    DOM.setStyleAttribute(w.getParent().getElement(), "cursor", "default");
                }*/
                int cursorY = event.getRelativeY(w.getElement());
                int initialY = w.getAbsoluteTop();
                int height = w.getOffsetHeight() - 50;
                int cursorX = event.getRelativeX(w.getElement());
                int initialX = w.getAbsoluteLeft();
                int width = w.getOffsetWidth();

                if (((initialX + width - 10) < cursorX && cursorX <= (initialX + width)) &&
                        ((initialY + height - 10) < cursorY && cursorY <= (initialY + height))) {
                    System.out.println("resize this");
                    DOM.setStyleAttribute(w.getParent().getElement(), "cursor", "se-resize");
                }

            }
        }, MouseOverEvent.getType());

    }

    /**
     * returns if mousepointer is in region to show cursor-resize
     *
     * @param event
     * @return true if in region
     */
    protected boolean isCursorResize(MouseEvent event, Widget w) {
        int cursorY = event.getRelativeY(w.getElement());
        int initialY = w.getAbsoluteTop();
        int height = w.getOffsetHeight() - 50;
        int cursorX = event.getRelativeX(w.getElement());
        int initialX = w.getAbsoluteLeft();
        int width = w.getOffsetWidth();

        if (((initialX + width - 10) < cursorX && cursorX <= (initialX + width)) &&
                ((initialY + height - 10) < cursorY && cursorY <= (initialY + height))) {
            System.out.println("resize this");
//            DOM.setStyleAttribute(w.getParent().getElement(), "cursor", "se-resize");
            return true;
        }else {
            return false;
        }
    }



}