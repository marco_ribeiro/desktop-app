package eurocontrol.sesarwp13.client.ui.panel;

/**
 * Created by mribeiro on 25/09/14.
 */
public abstract class InnerPanelHandler {
    public abstract void execute(InnerPanel innerPanel);
}
