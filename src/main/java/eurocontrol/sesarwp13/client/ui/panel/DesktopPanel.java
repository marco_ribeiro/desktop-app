/**
 * $$Id$$
 * @author mribeiro
 * @date 12/09/14 13:12
 *
 * Copyright (C) 2014 Eurocontrol
 *
 * All rights reserved.
 *
 */
package eurocontrol.sesarwp13.client.ui.panel;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import eurocontrol.sesarwp13.client.ui.image.ImageBundle;
import eurocontrol.sesarwp13.client.ui.menu.MenuHandler;
import eurocontrol.sesarwp13.client.ui.menu.TopMenuBar;
import eurocontrol.sesarwp13.client.ui.sytle.StyleBundle;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class DesktopPanel extends ResizeComposite {

    private final static int Z_INDEX = 2000;
    private TopMenuBar menuBar;
    private MenuHandler topMenuHandler;
    private int widgetCt = 0;
    //TODO: remove this map and use the map of each DesktopTabPanel
    private Map<String, InnerPanel> widgetMap;
    private InnerPanelHandler minimizeInnerPanelHandler;
    private InnerPanelHandler maximizeInnerPanelHandler;
    private InnerPanelHandler selectWindowHandler;
    private TabLayoutPanel tabLayoutPanel;
    private HashMap<Integer, DesktopTabPanel> tabs;
    private FlowPanel emptyDesktop;
    private LayoutPanel rootPanel;

    public DesktopPanel(MenuHandler menuHandler) {
        super();

        this.topMenuHandler = menuHandler;
        widgetMap = new HashMap<>();
        tabs = new HashMap<>();
        rootPanel = new LayoutPanel();
        tabLayoutPanel = createTabPanel();
        emptyDesktop = new FlowPanel();
        emptyDesktop.add(new HTML("<h1>Empty Desktop</h1>"));
        emptyDesktop.addStyleName("emptyDesktop");

        menuBar = new TopMenuBar(topMenuHandler);
        rootPanel.add(menuBar);
        rootPanel.add(tabLayoutPanel);
        rootPanel.add(emptyDesktop);
        rootPanel.setWidgetTopBottom(tabLayoutPanel, 1.8, Style.Unit.EM, 0, Style.Unit.EM);
        rootPanel.setWidgetTopBottom(emptyDesktop, 1.8, Style.Unit.EM, 0, Style.Unit.EM);
        initWidget(rootPanel);
        tabLayoutPanel.setVisible(false);

        //minimize inner panels handler
        minimizeInnerPanelHandler = new InnerPanelHandler() {
            @Override
            public void execute(InnerPanel innerPanel) {
                getCurrentTab().minimizeWidget(innerPanel);
            }
        };

        //maximize inner panels handler
        maximizeInnerPanelHandler = new InnerPanelHandler() {
            @Override
            public void execute(InnerPanel innerPanel) {
                getCurrentTab().maximizeWidget(innerPanel);
            }
        };

        //sets the z-index to the highest value
        selectWindowHandler = new InnerPanelHandler() {
            @Override
            public void execute(InnerPanel innerPanel) {
                resetZIndexes();
                int zIdx = getMaxZIndex();
                innerPanel.getElement().getStyle().setZIndex(zIdx + 1);
            }
        };
    }

    /**
     * Adds a new window to the current workspace
     *
     * @param widget - Panel that represents a new window
     */
    public void addWidget(InnerPanel widget) {
        if (tabLayoutPanel.getWidgetCount() == 0) {
            //TODO: replace this Window.alert by a dialogBox
            Window.alert("First you need to create an workspace");
            return;
        }
        DesktopTabPanel panel = (DesktopTabPanel) tabLayoutPanel.getWidget(tabLayoutPanel.getSelectedIndex());

        widgetCt++;
        widget.getElement().getStyle().setZIndex(Z_INDEX + widgetCt);
        widgetMap.put(widget.getElement().getId(), widget);
        widget.setMinimizeHandler(minimizeInnerPanelHandler);
        widget.setMaximizeHandler(maximizeInnerPanelHandler);
        widget.setSelectWindowHandler(selectWindowHandler);
        saveCurrentPositions();
        panel.add(widget);
        panel.getWidgetContainerElement(widget).getStyle().clearPosition();

        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                restorePositions();
            }
        });

    }

    /**
     * Creates a new workspace (tab).
     */
    public void addNewWorkspace() {
        //prompt user for workspace title
        //TODO: remove Window.prompt a replace by a dialogbox
        final String title = Window.prompt("Workspace title name:", "NewWorkspace");

        //user pressed cancel
        if (title == null || title.trim().isEmpty()) {
            return;
        }

        if (tabs.isEmpty()) {
            tabLayoutPanel.setVisible(true);
            emptyDesktop.removeFromParent();
        }

        //set new tab index
        final int newIndex = tabLayoutPanel.getWidgetCount();

        //create empty content to the tab
        DesktopTabPanel tabContent = createDesktopPanel();

        //handler when user press close tab button
        ClickHandler closeTabHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                tabLayoutPanel.remove(newIndex);
                tabs.remove(newIndex);
                if(tabs.isEmpty()){
                    tabLayoutPanel.setVisible(false);

                    //TODO: change the layoutpanel to a tabpanel without animation
                    rootPanel.add(emptyDesktop);
                    rootPanel.setWidgetTopBottom(emptyDesktop, 1.8, Style.Unit.EM, 0, Style.Unit.EM);
                }
            }
        };

        //add new tab
        tabLayoutPanel.add(tabContent, createTabTitle(title, closeTabHandler));

        //select new tab
        tabLayoutPanel.selectTab(newIndex);

        //add new tab content to the list
        tabs.put(newIndex, tabContent);
    }

    /**
     * Creates a panel with the title text and a button to close the workspace/tab
     * @param title - Title to appear on the tab
     * @param closeHandler - Tab close handler
     * @return - FlowPanel with
     */
    private FlowPanel createTabTitle(final String title, ClickHandler closeHandler){
        FlowPanel titlePanel = new FlowPanel();
        Label label = new Label(title);
        label.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
        titlePanel.add(label);

        Hyperlink closeButton = new Hyperlink();
        closeButton.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
        Image imageClose = new Image(ImageBundle.ICONS.cancel());
        imageClose.getElement().getStyle().setVerticalAlign(Style.VerticalAlign.MIDDLE);
        imageClose.getElement().getStyle().setMarginLeft(5.0, Style.Unit.PX);
        imageClose.setStyleName(StyleBundle.INSTANCE.myStyle().innerPanelRoundedButton());
        imageClose.setAltText("Close");
        closeButton.getElement().appendChild(imageClose.getElement());
        closeButton.addDomHandler(closeHandler, ClickEvent.getType());

        titlePanel.add(closeButton);
        return titlePanel;
    }

    /**
     * Creates a empty desktop panel
     * @return
     */
    private DesktopTabPanel createDesktopPanel() {
        DesktopTabPanel panel = new DesktopTabPanel();
        return panel;
    }

    /**
     * Save current widgets positions
     */
    private void saveCurrentPositions() {
        for (InnerPanel innerPanel : widgetMap.values()) {
            innerPanel.saveCurrentPosition();
        }
    }

    /**
     * Restores the widgets positions based on last saved positions.
     */
    private void restorePositions() {
        for (InnerPanel innerPanel : widgetMap.values()) {
            innerPanel.restorePosition();
        }
    }

    /**
     * Get the max Z-index of the widgets
     * @return
     */
    private int getMaxZIndex() {
        int maxZ = Integer.MIN_VALUE;
        for (InnerPanel w : widgetMap.values()) {
            if (w.getZIndex() > maxZ) {
                maxZ = w.getZIndex();
            }
        }
        return maxZ;
    }

    /**
     * to reuse z-indexes
     */
    private void resetZIndexes() {
        //order by z-index to maintain the current positions
        Map<Integer, InnerPanel> orderedPanels = new HashMap<>(widgetMap.size());
        for (InnerPanel w : widgetMap.values()) {
            orderedPanels.put(w.getZIndex(), w);
        }
        int initial = Z_INDEX;
        for (InnerPanel w : orderedPanels.values()) {
            w.getElement().getStyle().setZIndex(initial);
            initial++;
        }
    }

    private TabLayoutPanel createTabPanel() {
        TabLayoutPanel tabPanel = new TabLayoutPanel(2, Style.Unit.EM);
        tabPanel.setAnimationDuration(500);
        tabPanel.ensureDebugId("cwTabPanel");
        return tabPanel;
    }

    private DesktopTabPanel getCurrentTab() {
        return tabs.get(tabLayoutPanel.getSelectedIndex());
    }
}