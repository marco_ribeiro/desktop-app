/**
 * $$Id$$
 * @author mribeiro
 * @date 12/09/14 15:49
 *
 * Copyright (C) 2014 BTC-ATM
 *
 * All rights reserved.
 *
 */
package eurocontrol.sesarwp13.client.ui.panel.resizable;

/**
 * @author Vince Vice - www.electrosound.tv
 * This is licensed under Apache License Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0.html
 */
public interface PanelResizeListener {
    /**
     * indicates a Panel has been resized
     * @param width
     * @param height
     **/
    public void onResized(Integer width,Integer height);

}