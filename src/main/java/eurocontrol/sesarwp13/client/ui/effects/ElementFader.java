package eurocontrol.sesarwp13.client.ui.effects;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;

/**
 * Created by mribeiro on 25/09/14.
 */
public class ElementFader {
    private int stepCount;

    public ElementFader() {
        this.stepCount = 0;
    }

    private void incrementStep() {
        stepCount++;
    }

    private int getStepCount() {
        return stepCount;
    }

    public void fade(final Element element, final float startOpacity, final float endOpacity, int totalTimeMillis, final FaderCallback faderCallback) {

        final int numberOfSteps = 30;
        int stepLengthMillis = totalTimeMillis / numberOfSteps;

        stepCount = 0;

        final float deltaOpacity = (float) (endOpacity - startOpacity) / numberOfSteps;

        Timer timer = new Timer() {

            @Override
            public void run() {
                float opacity = startOpacity + (getStepCount() * deltaOpacity);
                element.getStyle().setOpacity(opacity);
//                DOM.setStyleAttribute(element, "opacity", Float.toString(opacity));

                incrementStep();
                if (getStepCount() == numberOfSteps) {
                    element.getStyle().setOpacity(opacity);
//                    DOM.setStyleAttribute(element, "opacity", Float.toString(endOpacity));
                    this.cancel();
                    faderCallback.fadeEnd();
                }
            }
        };

        timer.scheduleRepeating(stepLengthMillis);
    }

    public static abstract class FaderCallback {
        public abstract void fadeEnd();
    }
}
