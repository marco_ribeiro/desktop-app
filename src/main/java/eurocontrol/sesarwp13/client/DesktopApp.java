package eurocontrol.sesarwp13.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import eurocontrol.sesarwp13.client.ui.menu.MenuHandler;
import eurocontrol.sesarwp13.client.ui.menu.TopMenuBar;
import eurocontrol.sesarwp13.client.ui.panel.DesktopPanel;
import eurocontrol.sesarwp13.client.ui.panel.InnerPanel;
import eurocontrol.sesarwp13.client.ui.panel.InnerPanelHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class DesktopApp implements EntryPoint {

    private RootLayoutPanel rootPanel;
    private LayoutPanel  layoutPanel;
    private InnerPanel innerPanel;

    private DesktopPanel desktopPanel;

    private HashMap<String, Command> menu;
    private MenuHandler menuHandler;

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        rootPanel = RootLayoutPanel.get();
        RootPanel.get("MyRoot").add(rootPanel);

        layoutPanel = new LayoutPanel ();
//        layoutPanel.setStylePrimaryName("istamlayout");
        menuHandler = new MenuHandler() {
            @Override
            public void execute(String id) {
                if(menu.containsKey(id)){
                    menu.get(id).execute();
                }else {
                    Window.alert("Please contact administrator");
                }
            }
        };

        innerPanel = new InnerPanel("inner1");
        innerPanel.setPanelTitle("Inner Panel Test");

        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.add(new Label("Label: "));
        horizontalPanel.add(new TextBox());
        innerPanel.setHeight("300px");
        innerPanel.setWidth("300px");
        innerPanel.setInnerWidget(horizontalPanel);

        desktopPanel = new DesktopPanel(menuHandler);

//        desktopPanel.addWidget(draggableInnerPanel);

//        desktopPanel.addWidget(innerPanel);

        InnerPanel innerPanel2 = new InnerPanel("inner2");
        innerPanel2.setPanelTitle("Inner Panel Test2");

        HorizontalPanel horizontalPanel2 = new HorizontalPanel();
        horizontalPanel2.add(new Label("Label: "));
        horizontalPanel2.add(new TextBox());
        innerPanel2.setHeight("300px");
        innerPanel2.setWidth("300px");
        innerPanel2.setInnerWidget(horizontalPanel2);

//        desktopPanel.addWidget(innerPanel2);

        rootPanel.add(desktopPanel);

        initTopMenu();

    }

    private void createNewWidget(String id){
        if(id == null){
            id = DOM.createUniqueId();
        }
        InnerPanel innerPanel2 = new InnerPanel(DOM.createUniqueId());
        innerPanel2.setPanelTitle(id);

        HorizontalPanel horizontalPanel2 = new HorizontalPanel();
        horizontalPanel2.add(new Label("Label: "));
        horizontalPanel2.add(new TextBox());
        innerPanel2.setHeight("300px");
        innerPanel2.setWidth("300px");
        innerPanel2.setInnerWidget(horizontalPanel2);

        desktopPanel.addWidget(innerPanel2);
    }

    private void initTopMenu(){
        menu = new HashMap<>();
        Command menuNewWorkspaceCommand = new Command() {
            @Override
            public void execute() {
                desktopPanel.addNewWorkspace();
            }
        };

        Command menuNewWidgetCommand = new Command() {
            @Override
            public void execute() {
                createNewWidget(null);
            }
        };

        Command menuHelpAboutCommand = new Command() {
            @Override
            public void execute() {
                Window.alert("Eurocontrol 2014 - SESAR");
            }
        };
        menu.put("New Workspace", menuNewWorkspaceCommand);
        menu.put("New Window", menuNewWidgetCommand);
        menu.put("About", menuHelpAboutCommand);
    }

}
